<?php
require_once(__DIR__.'/../controller/IndexController.php');

$id = $_GET['c'];

$cnt = new IndexController();
$rb = $cnt->getAuthor($id);

?>

<html>
    <head>
        <title>Author's Update</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    </head>
    <body>
        <div>
            <h1>Updating <?=$rb->getName()?>...</h1>
            <form method="post" action="/forms/update.php">
        		<d1>
        		    <!-- Author -->
        		    <dt><label for="aauthor">Author</label></dt>
        		    <dd><input type="text" id="aauthor" name="aauthor" value="<?=$rb->getName()?>"/></dd>
        		    
        		    <!-- Quote -->
        		    <dt><label for="aquote">Quote</label></dt>
        		    <dd><input type="text" id="aquote" name="aquote" value="<?=$rb->getQuote()?>"/></dd>
        		    
         		    <!-- Submit -->
        		    <dd>
        		        <input type="hidden" name="aid" value="<?=$rb->getId()?>"/>
        		        <input type="submit" name="submit" value="Add"/>
                    </dd>
        		</d1>
            </form>
        </div>
    </body>
</html>