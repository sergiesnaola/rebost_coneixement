<?php
require_once (__DIR__.'/../controller/IndexController.php');

$cnt = new IndexController();
$list = $cnt->getListAuthors();

?><html>
    <head>
    <title>Quotes List</title>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    </head>
        <body>
        	<div id="wrapper">
        		<h1>Author & Quotes List (<a href="/insert.php">Add</a>) </h1>
        		<table>
        		    <tr>
        		        <th>Author</th>
        		        <th>Quote</th>
        		        <th>Date</th>
        		    </tr>
        			<?php foreach($list as $r){ ?>
        			<tr>
            			<td><a href="/details.php?c=<?=$r->getId()?>"><?=$r->getName()?></a></td>
            			<td><a href="/details.php?c=<?=$r->getId()?>"><?=$r->getQuote()?></a></td>
            			<td><a href="/details.php?c=<?=$r->getId()?>"><?=$r->getDate()?></a></td>
                        <td><a href="/update.php?c=<?=$r->getId()?>">Update</a></td>
            			<td><a href="/remove.php?c=<?=$r->getId()?>">Remove</a></td>
        			</tr>
        			<?php } ?>
        		</table>
        		
        	</div>
        </body>
</html>


