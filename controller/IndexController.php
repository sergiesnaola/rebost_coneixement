<?php
require_once (__DIR__.'/db/AuthorDb.php');

class IndexController{
   
   public function createAuthor($name, $quote){
       
       // Create Author instance 
       $rb = new Author($id, $name, $quote);
       
       // Instantiate Author
       $db = new AuthorDb();
       
       // Create DB registry and returm
       return $db->createAuthor($rb);
   }
   
   public function getListAuthors(){
       $db = new AuthorDb();
       return $db->listAuthors();
   }
   
   public function getAuthor($id){
       $db = new AuthorDb();
       return $db->getAuthor($id);
   }
   
   public function updateAuthor($name, $quote, $id){
      $db = new AuthorDb();
      return $db->updateAuthor($name, $quote, $id);
   }
   
   public function removeAuthor($id){
       $db = new AuthorDb();
       return $db->removeAuthor($id);
   }
    
}