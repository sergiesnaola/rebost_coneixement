<?php

require_once(__DIR__.'/../../model/Author.php');

class AuthorDb{
    
    private $_conn;
    
    
    // Create a db registry given a Author instance
    public function createAuthor($rebost){
        
        // Open connection
        $this->openConnection();
        
        // Prepare statement
        $insert = "INSERT INTO rebost.author (name, quote) VALUES (?, ?) ";
        $stmt = $this->_conn->prepare($insert);
        
        // Binding params
        $stmt->bind_param("ss", $a, $q);
        $a = $rebost->getName();
        $q = $rebost->getQuote();

        // Execute
        //$stmt->execute();
        
        if(!$stmt->execute()){
            echo "Execution fail: (" . $stmt->error . ") " . $insert->error;
            return false;
        } else{
            return $this->getAuthor($stmt->insert_id);
        }
        
       
       // return $this->getAuthor($stmt->insert_id);
    }
    
    // List all Author registries
    public function listAuthors(){
        
        // Open connection
        $this->openConnection();
        
        // Prepare statement
        $query = "SELECT * FROM rebost.author";
        $stmt = $this->_conn->prepare($query);
        
        // Execute
        $stmt->execute();
        $res = $stmt->get_result();
        
        // Processing results
        $result = array();
        while ($rb = $res->fetch_assoc()){
            // Create instance
            $rbinstance = new Author($rb['id'],$rb['name'], $rb['quote'], $rb['date']);
            array_push($result, $rbinstance);
        }
        return $result;
    }
    
    // Get a Author instance given it's ID
    public function getAuthor($ip){
        // Open connection
        $this->openConnection();
        
        // Prepare statement
        $query = "SELECT * FROM rebost.author WHERE id = ?";
        $stmt = $this->_conn->prepare($query);
        
        // Binding param
        $stmt->bind_param("i", $id);
        $id = $ip;
        //Execute
        $stmt->execute();
        $res = $stmt->get_result();
        // Processing results
        while ($rb = $res->fetch_assoc()){
            $rbinstance = new Author($rb['id'],$rb['name'], $rb['quote'], $rb['date']);
        }
        return $rbinstance;
        
    }
    
    //  Remove Author registry and return the original data
    public function removeAuthor($id){
        
        // Get instance
        $rb = $this->getAuthor($id);

        // Open connection
        $this->openConnection();
    
        // Prepare statement
        $query = "DELETE FROM rebost.author WHERE id = ? ";
        $stmt = $this->_conn->prepare($query);
        
        // Binding param
        $stmt->bind_param("i", $rbid);
        $rbid = $id;
        
        // Execute
        $stmt->execute();
        return $rb;
        
    }
    
    public function updateAuthor($n, $q, $i){
        $date = new DateTime("NOW");

        // Open connection
        $this->openConnection();

        // Prepare statement
        $query = "UPDATE rebost.author SET name = ?, quote = ?, date = '{$date->format('Y-m-d H:i:s')}' WHERE id = ?";
        $stmt = $this->_conn->prepare($query);
        
        // Binding params
        $stmt->bind_param("ssi", $an, $aq, $ai);
        $an = $n;
        $aq = $q;
        $ai = $i;
        
        // Execute
        $stmt->execute();
        return $this->getAuthor($i);
    }
    
    // Opens connection with the database
    private function openConnection () {
        if($this->_conn == null){
            $this->_conn = mysqli_connect("localhost", //servername 
                                        "sergi",   // user
                                        "admin",   // password
                                        "rebost"); // database
        }
    }
    
    
}


