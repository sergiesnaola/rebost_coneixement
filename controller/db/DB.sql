CREATE DATABASE rebost;
CREATE USER 'sergi'@'localhost' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON rebost.* TO 'sergi'@'localhost';
CREATE TABLE rebost.author (id int NOT NULL AUTO_INCREMENT, name VARCHAR(20), quote VARCHAR(255), PRIMARY KEY (id));
ALTER TABLE rebost.author ADD COLUMN date timestamp NOT NULL default current_timestamp;