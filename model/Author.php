<?php

class Author
{

    private $_id;
    private $_name;
    private $_quote;
    private $_date;

    public function __construct($id=null, $name=null, $quote=null, $date=null){
        $this->setId($id);
        $this->setName($name);
        $this->setQuote($quote);
        $this->setDate($date);
    }
    
    public function getId(){
        return $this->_id;
    }

    public function getName(){
        return $this->_name;
    }

    public function getQuote(){
        return $this->_quote;
    }
    
    public function getDate(){
        return $this->_date;
    }

    public function setId($id){
        $this->_id = $id;
    }
    
    public function setName($name){
        $this->_name = $name;
    }
    
    public function setQuote($quote){
        $this->_quote = $quote;
    }
    
    public function setDate($date){
        $this->_date = $date;
    }
}